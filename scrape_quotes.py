from bs4 import BeautifulSoup as bs
import urllib.request
import re

URL = 'https://en.wikiquote.org/wiki/Death'
soup = bs(urllib.request.urlopen(URL), 'html.parser')
# Get contents of wiki page
soup = soup.find(class_='mw-parser-output')
# Get all quotes and authors.
# Quotes are <ul>s one level down, and the author is a <dl> that immediately
# follows.
quotes = soup.find_all('ul', recursive=False)
authors = [q.find_next_sibling('dl') for q in quotes]

# The actual quote is a <li> inside the <ul> (and same for authors)
quotes_and_authors = []
for q, a in zip(quotes, authors):
    if a and q:
        quote = q.find('li').text.strip()
        try:
            author = a.find('li').text.strip()
        except:
            continue
        quotes_and_authors.append((quote, author))

for q, a in quotes_and_authors:
    # Alexa can't speak more than 8k characters in a response
    if len(q) <= 8000 and (len(a) > 0 and len(q) > 0):
        print(q)
        print('---')
        print(a)
        print('------\n\n')
