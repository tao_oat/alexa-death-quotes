# Death Quotes Alexa Skill

This is a skill for the Amazon Echo that gives you a quote about death.

Quotes are sourced from [Wikiquote](https://en.wikiquote.org/wiki/Death). These
quotes are available under a [Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/3.0/).
