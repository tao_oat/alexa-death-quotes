quotes = open('quotes.txt').read().split('------')
quotes = [q.split('---') for q in quotes]
quotes = [[item.strip().replace('\n', ', ').replace('p.', 'page') for item in qa] for qa in quotes]

# Quotes longer than this characters are a bit too verbose
quotes = [qa for qa in quotes if len(qa[0]) < 350]

script = 'import random\nimport json\n\nQUOTES = [\n'
for i, (quote, author) in enumerate(quotes):
    script += '("""\n' + quote + '\n""",\n"""\n' + author + '\n""")'
    if i < len(quotes) - 1:
        script += ',\n'
script += '\n]'

script += """

HELP_TEXT = "The Death Quotes skill does one thing well: gives you a quote " +\\
            "about death. Try saying something like Alexa, open Death Quotes."

def handler(event, context):
    output_speech = random.choice(QUOTES)[0].strip()
    # Check if we got a help request
    if event['request']['type'] == 'IntentRequest':
        if event['request']['intent']['name'] == 'AMAZON.HelpIntent':
            output_speech = HELP_TEXT
    return {
        'version': '1.0',
        'response': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': output_speech
                }
            }
        }
"""

print(script)
