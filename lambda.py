import random
import json

QUOTES = [
("""
Death is a black camel, which kneels at the gates of all.
""",
"""
Abd-el-Kader, as reported in The Cyclopædia of Practical Quotations edited by Jehiel Keeler Hoyt (1882), page 79
"""),
("""
Call no man happy till he is dead.
""",
"""
Æschylus, Agamemnon, 938. Earliest reference. Also in Sophocles—Trachiniæ, and Œdipus Tyrannus
"""),
("""
But when the sun in all his state,, Illumed the eastern skies,, She passed through glory's morning gate,, And walked in Paradise.
""",
"""
James Aldrich, A Death Bed, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Somewhere, in desolate, wind-swept space,, In twilight land, in no man's land,, Two hurrying shapes met face to face, And bade each other stand., "And who are you?" cried one, a-gape,, Shuddering in the glimmering light., "I know not," said the second shape,, "I only died last night."
""",
"""
Thomas Bailey Aldrich, Identity, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The white sail of his soul has rounded, The promontory—death.
""",
"""
William Alexander, The Icebound Ship, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Every breath you take is a step towards death.
""",
"""
Ali, Nahj al-Balagha, Sayings: 74
"""),
("""
In its flight from death, the craving for permanence clings to the very things sure to be lost in death.
""",
"""
Hannah Arendt, Love and Saint Augustine (1929), edited by Joanna Vecchiarelli Scott and Judith Chelius Stark (Chicago: 1996), page 17
"""),
("""
Your lost friends are not dead, but gone before,, Advanced a stage or two upon that road, Which you must travel in the steps they trod.
""",
"""
Aristophanes, Fragment, II; translation by Cumberland, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Her cabined ample spirit,, It fluttered and failed for breath;, Tonight it doth inherit, The vasty hall of death.
""",
"""
Matthew Arnold, Requiescat, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Our bodies are prisons for our souls. Our skin and blood, the iron bars of confinement. But fear not. All flesh decays. Death turns all to ash. And thus, death frees every soul.
""",
"""
Darren Aronofsky, in lines for the Grand Inquisitor Silecio, in The Fountain
"""),
("""
It is not death that a man should fear, but never beginning to live.
""",
"""
Marcus Aurelius, Meditations
"""),
("""
Death is a friend of ours; and he that is not ready to entertain him is not at home.
""",
"""
Francis Bacon, An Essay on Death published in The Remaines of the Right Honourable Francis Lord Verulam (1648) but may not have been written by Bacon
"""),
("""
Men fear death, as children fear to go in the dark; and as that natural fear in children is increased with tales, so is the other.
""",
"""
Francis Bacon, Essays, 2, 'Of Death'
"""),
("""
I have often thought upon death, and I find it the least of all evils.
""",
"""
Francis Bacon, An Essay on Death
"""),
("""
The pomp of death alarms us more than death itself.
""",
"""
Quoted by Francis Bacon as from Seneca, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
It is as natural to die as to be born; and to a little infant, perhaps, the one is as painful as the other.
""",
"""
Francis Bacon, Essays, Of Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
What then remains, but that we still should cry, Not to be born, or being born to die.
""",
"""
Ascribed to Francis Bacon; paraphrase of a Greek epigram, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Life hath more awe than death.
""",
"""
Philip James Bailey, Festus (1839), scene Wood and Water
"""),
("""
Death is the universal salt of states;, Blood is the base of all things — law and war.
""",
"""
Philip James Bailey, Festus (1839), scene A Country Town
"""),
("""
So fades a summer cloud away;, So sinks the gale when storms are over;, So gently shuts the eye of day;, So dies a wave along the shore.
""",
"""
Anna Letitia Barbauld, The Death of the Virtuous, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
It is only the dead who do not return.
""",
"""
Bertrand Barère, speech (1794), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
To die would be an awfully big adventure.
""",
"""
J. M. Barrie, Peter Pan, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
But whether on the scaffold high,, Or in the battle's van,, The fittest place where man can die, Is where he dies for man.
""",
"""
Michael J. Barry, The Place to Die, in The Dublin Nation (Sept. 28, 1844), Volume II, page 809
"""),
("""
To the Christian, these shades are the golden haze which heaven's light makes, when it meets the earth, and mingles with its shadows.
""",
"""
Henry Ward Beecher, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 176
"""),
("""
And when no longer we can see Thee, may we reach out our hands, and find Thee leading us through death to immortality and glory.
""",
"""
Henry Ward Beecher, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 179
"""),
("""
So we fall asleep in Jesus. We have played long enough at the games of life, and at last we feel the approach of death. We are tired out, and we lay our heads back on the bosom of Christ, and quietly fall asleepage
""",
"""
Henry Ward Beecher, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 184
"""),
("""
For certain is death for the born, And certain is birth for the dead;, Therefore over the inevitable, Thou shouldst not grieve.
""",
"""
Bhagavad Gita, Chapter 2
"""),
("""
Never the spirit was born, the spirit shall cease to be never. Never was time it was not, end and beginning are dreams.
""",
"""
Bhagavad Gita
"""),
("""
Generals gathered in their masses, just like witches at black masses., Evil minds that plot destruction,, sorcerer of death's construction.
""",
"""
Black Sabbath War Pigs Paranoid written by Ozzy Osbourne, Tony Iommi, Geezer Butler and Bill Ward
"""),
("""
How shocking must thy summons be, O Death!, To him that is at ease in his possessions!, Who, counting on long years of pleasure here,, Is quite unfurnished for the world to come!, In that dread moment, how the frantic soul, Raves round the walls of her clay tenement;, Runs to each avenue, and shrieks for help;, But shrieks in vain.
""",
"""
Robert Blair, The Grave, line 350
"""),
("""
Sure 'tis a serious thing to die! My soul!, What a strange moment must it be, when, near, Thy journey's end, thou hast the gulf in view!, That awful gulf, no mortal ever repassed, To tell what's doing on the other side.
""",
"""
Robert Blair, The Grave, line 369
"""),
("""
'Tis long since Death had the majority.
""",
"""
Robert Blair, The Grave, line 451. Please "The Great Majority" found in Plautus. Trinium, II. 214
"""),
("""
All our times have come, Here but now they're gone, Seasons don't fear the reaper, Nor do the wind, the sun or the rain... we can be like they are, Come on baby... don't fear the reaper, Baby take my hand... don't fear the reaper, We'll be able to fly... don't fear the reaper, Baby I'm your man...
""",
"""
Blue Oyster Cult (Don't Fear) The Reaper| written by Buck Dharma
"""),
("""
When I lived, I provided for every thing but death; now I must die, and am unprepared.
""",
"""
Caesar Borgia, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 176
"""),
("""
Beyond the shining and the shading, I shall be soon., Beyond the hoping and the dreading, I shall be soon., Love, rest and home—, Lord! tarry not, but come.
""",
"""
Horatius Bonar, Beyond the Smiling and the Weeping, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Earth to earth, ashes to ashes, dust to dust, in sure and certain hope of the resurrection.
""",
"""
Book of Common Prayer, Burial of the Dead, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Man that is born of a woman hath but a short time to live, and is full of misery. He cometh up, and is cut down, like a flower; he fleeth as it were a shadow, and never continueth in one stay.
""",
"""
Book of Common Prayer, Burial of the Dead; quoting from Job, XIV. 1, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
In the midst of life we are in death.
""",
"""
Book of Common Prayer, Burial of the Dead. Media vita in morte sumus. From a Latin antiphon. Found in the choirbook of the monks of St. Gall. Said to have been composed by Notker ("The Stammerer") in 911, while watching some workmen building a bridge at Martinsbrücke, in peril of their lives. Luther's antiphon "De Morte" Hymn XVIII is taken from this, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Here. Astride the top of nothingness, I suddenly receive the call of death. Who, in passing, tells me that it’s nothing. Nothing more than the absence of the word itself. Noting more, and simply nothingness.
""",
"""
Giannina Braschi, Empire of Dreams (1988)
"""),
("""
'Mid youth and song, feasting and carnival,, Through laughter, through the roses, as of old, Comes Death, on shadowy and relentless feet, Death, unappeasable by prayer or gold;, Death is the end, the end., Proud, then, clear-eyed and laughing, go to greet, Death as a friend!
""",
"""
Rupert Brooke, Second Best, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Oh! death will find me, long before I tire, Of watching you; and swing me suddenly, Into the shade and loneliness and mire, Of the last land!
""",
"""
Rupert Brooke, Sonnet (Collection 1908–1911), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
A little before you made a leap in the dark.
""",
"""
Sir Thomas Browne, Works, II, 26 (1708 edition); Letters from the Dead (1701), Works, II, page 502
"""),
("""
The thousand doors that lead to death.
""",
"""
Sir Thomas Browne, Religio Medici (1642), Part I, Section XLIV
"""),
("""
We all labour against our own cure; for death is the cure of all disease.
""",
"""
Thomas Browne, Religio Medici (1642), Part II, Section IX
"""),
("""
Pliny hath an odd and remarkable Passage concerning the Death of Men and Animals upon the Recess or Ebb of the Sea.
""",
"""
Sir Thomas Browne, Letter to a Friend, Section 7
"""),
("""
O Earth, so full of dreary noises!, O men, with wailing in your voices!, O delved gold, the waller's heap!, O strife, O curse, that over it fall!, God makes a silence through you all,, And "giveth His beloved, sleepage"
""",
"""
Elizabeth Browning, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 183
"""),
("""
For I say, this is death and the sole death,, When a man's loss comes to him from his gain,, Darkness from light, from knowledge ignorance,, And lack of love from love made manifest.
""",
"""
Robert Browning, A Death in the Desert, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The grand perhaps.
""",
"""
Robert Browning, Bishop Blougram's Apology, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Fear Death? – to feel the fog in my throat,, The mist in my face.
""",
"""
Robert Browning, Prospice
"""),
("""
Sustained and soothed, By an unfaltering trust, approach thy grave, Like one that wraps the drapery of his couch, About him, and lies down to pleasant dreams.
""",
"""
William Cullen Bryant, Thanatopsis
"""),
("""
All that tread, The globe are but a handful to the tribes, That slumber in its bosom.
""",
"""
William Cullen Bryant, Thanatopsis
"""),
("""
So he passed over and all the trumpets sounded, For him on the other side.
""",
"""
John Bunyan, Pilgrim's Progress. Death of Valiant for Truth. Close of Part II
"""),
("""
The dead ride swiftly.
""",
"""
Gottfried Bürger, Leonore, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
But, oh! fell Death's untimely frost,, That nipt my flower sae early.
""",
"""
Robert Burns, Highland Mary, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
There is only rest and peace, In the city of Surcease, From the failings and the waitings 'neath the sun,, And the wings of the swift years, Beat but gently over the biers, Making music to the sleepers every one.
""",
"""
Richard Eugene Burton, City of the Dead, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
They do neither plight nor wed, In the city of the dead,, In the city where they sleep away the hours.
""",
"""
Richard Eugene Burton, City of the Dead, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
We wonder if this can be really the close,, Life's fever cooled by death's trance;, And we cry, though it seems to our dearest of foes,, "God give us another chance."
""",
"""
Richard Eugene Burton, Song of the Unsuccessful, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The fear of death is worse than death.
""",
"""
Robert Burton, The Anatomy of Melancholy (1621)
"""),
("""
Friend Ralph! thou hast, Outrun the constable at last!
""",
"""
Samuel Butler, Hudibras, Part I (1663-64), Canto III, line 1,367
"""),
("""
Heaven gives its favourites — early death.
""",
"""
Lord Byron, Childe Harold's Pilgrimage, Canto IV (1818), Stanza 102. Also Don Juan, Canto IV, Stanza 12
"""),
("""
Without a grave, unknelled, uncoffined, and unknown.
""",
"""
Lord Byron, Childe Harold's Pilgrimage, Canto IV (1818), Stanza 179
"""),
("""
Ah! surely nothing dies but something mourns!
""",
"""
Lord Byron, Don Juan (1818-24), Canto III, Stanza 108
"""),
("""
"Whom the gods love die young," was said of yore.
""",
"""
Lord Byron, Don Juan (1818-24), Canto IV, Stanza 12
"""),
("""
Death, so called, is a thing which makes men weep,, And yet a third of life is passed in sleepage
""",
"""
Lord Byron, Don Juan (1818-24), Canto XIV, Stanza 3
"""),
("""
Oh, God! it is a fearful thing, To see the human soul take wing, In any shape, in any mood!
""",
"""
Lord Byron, Prisoner of Chillon, Stanza 8
"""),
("""
Down to the dust! — and, as thou rott'st away,, Even worms shall perish on thy poisonous clay.
""",
"""
Lord Byron, A Sketch
"""),
("""
But this we may positively state, that nobody has made any progress in the school of Christ unless he cheerfully looks forward to the day of his death and to the day of the final resurrection.
""",
"""
John Calvin Golden Booklet of the True Christian Life, pg. 78
"""),
("""
To live in hearts we leave behind, is not to die.
""",
"""
Thomas Campbell, Hallowed Ground (1825)
"""),
("""
Brougham delivered a very warm panegyric upon the ex-Chancellor, and expressed a hope that he would make a good end, although to an expiring Chancellor death was now armed with a new terror.
""",
"""
Thomas Campbell, Lives of the Chancellors, Volume VII, page 163
"""),
("""
And I still onward haste to my last night;, Time's fatal wings do ever forward fly;, So every day we live, a day we die.
""",
"""
Thomas Campion, Divine and Moral Songs, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Oh man, at that end not much has been left of your excellence, nothing of all that you have been boasting about through life - only sex, fear, self-admiration and a few other things you are usually ashamed of.
""",
"""
Karel Čapek, "Last Things of Man" (Stories from the Second Pocket, 1932)
"""),
("""
His religion, at best, is an anxious wish; like that of Rabelais, "a great Perhaps."
""",
"""
Thomas Carlyle, Burns, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Who now travels that dark path from whose bourne they say no one returns.
""",
"""
Catullus, Carmina, III. 11
"""),
("""
Suns may set and rise; we, when our short day has closed, must sleep on during one neverending night.
""",
"""
Catullus, Carmina. V. 4
"""),
("""
When death hath poured oblivion through my veins,, And brought me home, as all are brought, to lie, In that vast house, common to serfs and thanes,—, I shall not die, I shall not utterly die,, For beauty born of beauty—that remains.
""",
"""
Madison Cawein, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
If sudden, the visit of the Grim Reaper cannot be unwelcome.
""",
"""
Fausto Cercignani in: Brian Morris, Simply Transcribed. Quotations from Writings by Fausto Cercignani, 2014, quote 53
"""),
("""
“Love the others and you will be loved!” is a saying that might sound as a terrible and unjust accusation against all the innocents that have been hated and perhaps even tortured and killed.
""",
"""
Fausto Cercignani in: Brian Morris, Simply Transcribed. Quotations from Writings by Fausto Cercignani, 2014, quote 58
"""),
("""
"For all that let me tell thee, brother Panza," said Don Quixote, "that there is no recollection which time does not put an end to, and no pain which death does not remove.", "And what greater misfortune can there be," replied Panza, "than the one that waits for time to put an end to it and death to remove it?"
""",
"""
Miguel de Cervantes, Don Quixote (1605-15), Part I, Chapter XV
"""),
("""
It singeth low in every heart,, We hear it each and all,—, A song of those who answer not,, However we may call;, They throng (he silence of the breast,, We see them as of yore,—, The kind, the brave, the true, the sweet,, Who walk with us no more.
""",
"""
John W. Chadwick, Auld Lang Syne, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
This character wherewith we sink into the grave at death is the very character wherewith we shall reappear at the resurrection.
""",
"""
Thomas Chalmers, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
When I hear it contended that the least sensitive are, on the whole, the most happy, I recall the Indian proverb: “It’s better to sit than to stand, it is better lie down than to sit, but death is best of all.”
""",
"""
Nicolas Chamfort, Maxims and Considerations, E. P. Mathers, trans. (1926), #155 (Parmée 131)
"""),
("""
Death is not the end. Death can never be the end. Death is the road. Life is the traveller. The soul is the guide.
""",
"""
Sri Chinmoy, My Rose Petals (1971)
"""),
("""
At length, fatigued with life, he bravely fell,, And health with Boerhaave bade the world farewell.
""",
"""
Benjamin Church, The Choice (1754)
"""),
("""
I depart from life as from an inn, and not as from my home.
""",
"""
Cicero, De Senectute, 23
"""),
("""
I do not wish to die: but I care not if I were dead.
""",
"""
Cicero, Tusculanarum Disputationum, I. 8. translation of verse of Epicharmus
"""),
("""
The divinity who rules within us, forbids us to leave this world without his command.
""",
"""
Cicero, Tusculanarum Disputationum, I. 30
"""),
("""
There are countless roads on all sides to the grave.
""",
"""
Cicero, Tusculanarum Disputationum, I. 43
"""),
("""
That last day does not bring extinction to us, but change of place.
""",
"""
Cicero, Tusculanarum Disputationum, I. 49
"""),
("""
Death, to a good man is but passing through a dark entry, out of one little dusky room of his Father's house into another that is fair and large, lightsome and glorious, and divinely entertaining.
""",
"""
Adam Clarke, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 178
"""),
("""
Death levels all things.
""",
"""
Claudianus, De Raptu Proserpinæ, II. 302
"""),
("""
Our souls are prisoners of the terror of death, and the day is beautiful.
""",
"""
Paulo Coelho, in Quinta Montanha [The Fifth Mountain](1998), Ch. 1
"""),
("""
Bloody pumps, face flat on the concrete. Here comes the white sheet. Mister Coroner, caught with some yellow tape, but the murderers escaped.
""",
"""
Eric Dwayne Collins, "Lyrical Gangbang" (1992), The Chronic (December 1992), Death Row Records
"""),
("""
How well he fell asleep!, Like some proud river, widening toward the sea;, Calmly and grandly, silently and deep,, Life joined eternity.
""",
"""
Samuel Taylor Coleridge, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 183
"""),
("""
Death levels master and slave, the sceptre and the law, and makes the unlike like.
""",
"""
Walter Colman, La Danse Machabre or Death's Duell (c. 1633)
"""),
("""
Death is like thunder in two particulars; we are alarmed at the sound of it; and it is formidable only from that which preceded it.
""",
"""
Charles Caleb Colton, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
Death comes with a crawl or he comes with a pounce,, And whether he's slow, or spry,, It isn't the fact that you're dead that counts,, But only, how did you die?
""",
"""
Edmund Vance Cooke, How Did You Die?, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He who does not fear death cares naught for threats.
""",
"""
Pierre Corneille, Le Cid, II. 1
"""),
("""
So that he seemed to depart not from life, but from one home to another.
""",
"""
Cornelius Nepos, Atticus, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
And all you men, whom greatness does so please,
""",
"""
Abraham Cowley, “Of Greatness”
"""),
("""
All flesh is grass, and all its glory fades, Like the fair flower dishevelled in the wind;, Riches have wings, and grandeur is a dream;, The man we celebrate must find a tomb,, And we that worship him, ignoble graves.
""",
"""
William Cowper, The Task (1785), Book III, line 261
"""),
("""
All has its date below; the fatal hour, Was registered in Heav'n ere time began., We turn to dust, and all our mightiest works, Die too.
""",
"""
William Cowper, The Task (1785), Book V, The Winter Morning Walk, line 540
"""),
("""
Two hands upon the breast,, And labor's done;, Two pale feet crossed in rest,, The race is won.
""",
"""
Dinah Craik, Now and Afterwards
"""),
("""
Life, that dares send, A challenge to his end,, And when it comes, say, "Welcome, friend!"
""",
"""
Richard Crashaw, Wishes to his (Supposed) Mistress, Stanza 29
"""),
("""
She'l bargain with them; and will giue, Them GOD; teach them how to liue, In him; or if they this deny,, For him she'l teach them how to Dy.
""",
"""
Richard Crashaw, Hymn to the Name and Honor of Saint Teresa, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
We are born, then cry,, We know not for why,, And all our lives long, Still but the same song.
""",
"""
Nathaniel Crouch (attributed), in Fly Leaves (pub. 1854), taken from Bristol Drollery (1674)
"""),
("""
The most heaven-like spots I have ever visited, have been certain rooms in which Christ's disciples were awaiting the summons of death. So far from being a "house of mourning," I have often found such a house to be a vestibule of glory.
""",
"""
Theodore L. Cuyler, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 183
"""),
("""
Death is utterly acceptable to consciousness and life. There has been endless times of numberless deaths, but neither consciousness nor life has ceased to arise. The felt quality and cycle to death has not modified the fragility of flowers, even the flowers within the human body.
""",
"""
Adi Da, "Prologue", The Knee of Listening
"""),
("""
Round, round the cypress bier, Where she lies sleeping,, On every turf a tear,, Let us go weeping!, Wail!
""",
"""
George Darley, Dirge, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
And though mine arm should conquer twenty worlds,, There's a lean fellow beats all conquerors.
""",
"""
Thomas Dekker, Old Fortunatus (1599), Act I, scene 1
"""),
("""
I expressed just now my mistrust of what is called Spiritualism—… I owe it a trifle for a message said to come from Voltaire's Ghost. It was asked, "Are you not now convinced of another world?" and rapped out, "There is no other world — Death is only an incident in Life."
""",
"""
William De Morgan, Joseph Vance, Chapter XI
"""),
("""
Death carries off a man who is gathering flowers and whose mind is distracted, as a flood carries off a sleeping village.
""",
"""
Dhammapada, Verse 47; F. Max Müller, translator
"""),
("""
What argufies pride and ambition?, Soon or late death will take us in tow:, Each bullet has got its commission,, And when our time's come we must go.
""",
"""
Charles Dibdin, Each Bullet has its Commission
"""),
("""
"People can't die, along the coast," said Mr. Peggotty, "except when the tide's pretty nigh out. They can't be born, unless it's pretty nigh in—not properly born, till flood. He's a-going out with the tide."
""",
"""
Charles Dickens, David Copperfield (1849-1850), Chapter XXX
"""),
("""
Soon for me the light of day, Shall forever pass away;, Then from sin and sorrow free,, Take me, Lord, to dwell with Thee.
""",
"""
William Croswell Doane, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
Welcome, thou kind deceiver!, Thou best of thieves! who, with an easy key,, Dost open life, and, unperceived by us,, Even steal us from ourselves.
""",
"""
John Dryden, All for Love, Act V, scene 1
"""),
("""
Death in itself is nothing; but we fear, To be we know not what, we know not where.
""",
"""
John Dryden, Aurengzebe, Act IV, scene 1
"""),
("""
So was she soon exhaled, and vanished hence;, As a sweet odour, of a vast expense., She vanished, we can scarcely say she died.
""",
"""
John Dryden, Elegiacs, To the Memory of Mrs. Anne Killegrew, line 303
"""),
("""
Of no distemper, of no blast he died,, But fell like autumn fruit that mellowed long.
""",
"""
John Dryden, Œdipus, Act IV, scene 1, line 265
"""),
("""
Heaven gave him all at once; then snatched away,, Ere mortals all his beauties could survey;, Just like the flower that buds and withers in a day.
""",
"""
John Dryden, On the Death of Amyntas, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He was exhaled; his great Creator drew, His spirit, as the sun the morning dew.
""",
"""
John Dryden, On the Death of a Very Young Gentleman, line 25
"""),
("""
Like a led victim, to my death I'll go,, And dying, bless the hand that gave the blow.
""",
"""
John Dryden, The Spanish Friar, Act II, scene 1, line 64
"""),
("""
In the jaws of death.
""",
"""
Guillaume de Salluste Du Bartas, Divine Weekes and Workes, Second Week, First day
"""),
("""
One event happeneth to them all.
""",
"""
Ecclesiastes, II. 14
"""),
("""
The grasshopper shall be a burden, and desire shall fail; because man goeth to his long home, and the mourners go about the streets.
""",
"""
Ecclesiastes, XII. 5
"""),
("""
Judge none blessed before his death.
""",
"""
Ecclesiasticus, XI. 28
"""),
("""
Death is the king of this world: 'tis his park, Where he breeds life to feed him. Cries of pain, Are music for his banquet.
""",
"""
George Eliot, Spanish Gypsy (1868), Book II
"""),
("""
If we could know, Which of us, darling, would be first to go,, Who would be first to breast the swelling tide, And step alone upon the other side—, If we could know!
""",
"""
Mrs. Foster Ely, If We could Know, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He thought it happier to be dead,, To die for Beauty, than live for bread.
""",
"""
Ralph Waldo Emerson, Beauty, line 25
"""),
("""
But learn that to die is a debt we must all pay.
""",
"""
Euripides, Alcestis, 418. Also Andromache, 1,271
"""),
("""
Out of the strain of the Doing,, Into the peace of the Done;, Out in the thirst of Pursuing,, Into the rapture of Won., Out of grey mist into brightness,, Out of pale dusk into Dawn—, Out of all wrong into rightness,, We from these fields shall be gone., "Nay," say the saints, "Not gone but come,, Into eternity's Harvest Home."
""",
"""
W. M. L. Fay, Poem in Sunday at Home (May, 1910), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He that always waits upon God is ready whenever He calls. Neglect not to set your accounts even; he is a happy man who to lives as that death at all times may find him at leisure to die.
""",
"""
Owen Feltham, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
I am not in the least surprised that your impression of death becomes more lively, in proportion as age and infirmity bring it nearer. God makes use of this rough trial to undeceive us in respect to our courage, to make us feel our weakness, and to keep us in all humility in His hands.
""",
"""
François Fénelon, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 185
"""),
("""
Sit the comedy out, and that done,, When the Play's at an end, let the Curtain fall down.
""",
"""
Thomas Flatman, The Whim, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Young Never-Grow-Old, with your heart of gold, And the dear boy's face upon you;, It is hard to tell, though we know it well,, That the grass is growing upon you.
""",
"""
Alice Fleming, Spion Kop, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death hath so many doors to let out life.
""",
"""
John Fletcher and Philip Massinger, The Custom of the Country (c. 1619–23; published 1647), Act II, scene 2
"""),
("""
We must all die!, All leave ourselves, it matters not where, when,, Nor how, so we die well; and can that man that does so, Need lamentation for him?
""",
"""
John Fletcher, Valentinian (1610–14; published 1647), Act IV, scene 4
"""),
("""
"Paid the debt of nature." No; it is not paying a debt; it is rather like bringing a note to the bank to obtain solid gold for it. In this case you bring this cumbrous body which is nothing worth, and which you could not wish to retain long; you lay it down, and receive for it from the eternal treasures — liberty, victory, knowledge, rapture.
""",
"""
John Foster, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 179
"""),
("""
When you take the wires of the cage apart, you do not hurt the bird, but help it. You let it out of its prison. How do vou know that death does not help me when it takes the wires of my cage down? — that it does not release me, and put me into some better place, and better condition of life?
""",
"""
Bishop Randolph S. Foster, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 184
"""),
("""
If there is a meaning in life at all, then there must be a meaning in suffering. Suffering is an ineradicable part of life, even as fate and death.
""",
"""
Viktor Frankl, in JW.org ; Why We Are Here
"""),
("""
A dying man can do nothing easy.
""",
"""
Benjamin Franklin, last words, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The mountain is passed; now we shall get on better.
""",
"""
Frederick the Great, said to be his last words, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Why fear death? It is the most beautiful adventure in life.
""",
"""
Charles Frohman, last words before he sank in the wreck of the Lusitania, torpedoed by the Germans (7 May 1915), as reported through Rita Joliet, in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Drawing near her death, she sent most pious thoughts as harbingers to heaven; and her soul saw a glimpse of happiness through the chinks of her sicknesse broken body.
""",
"""
Thomas Fuller, The Holy State and the Profane State, Book I, Chapter II
"""),
("""
Life's race well run,, Life's work well done,, Life's crown well won,, Now comes rest.
""",
"""
Epitaph of President James Garfield, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
To die is landing on some silent shore,, Where billows never break nor tempests roar;, Ere well we feel the friendly stroke 'tis over.
""",
"""
Sir Samuel Garth, The Dispensary (1699), Canto III, line 225
"""),
("""
The prince who kept the world in awe,, The judge whose dictate fixed the law;, The rich, the poor, the great, the small,, Are levelled; death confounds 'em all.
""",
"""
John Gay, Fables (1727), Part II. Fable 16
"""),
("""
Dead as a door nail.
""",
"""
John Gay, New Song of New Similes. Langland, Piers Ploughman, II, line 183. (1362). William of Palerne, Romance (About 1350), II Henry IV, Act V, scene 3. Deaf as a door nail. Rabelais, III. 34; translation. by Urquhart
"""),
("""
Where the brass knocker, wrapt in flannel band,, Forbids the thunder of the footman's hand,, The' upholder, rueful harbinger of death,, Waits with impatience for the dying breath.
""",
"""
John Gay, Trivia, Book II, line 467
"""),
("""
For dust thou art, and unto dust shall thou return.
""",
"""
Genesis, III. 19
"""),
("""
What if thou be saint or sinner,, Crooked gray-beard, straight beginner,—, Empty paunch, or jolly dinner,, When Death thee shall call., All alike are rich and richer,, King with crown, and cross-legged stitcher,, When the grave hides all.
""",
"""
R. W. Gilder, Drinking Song, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
None who ever knew her can believe her dead;, Though, should she die, they deem it well might be, Her spirit took its everlasting flight, In summer's glory, by the sunset sea,, That onward through the Golden Gate is fled., Ah, where that bright soul is cannot be night.
""",
"""
R. W. Gilder, "H. H."
"""),
("""
Can storied urn or animated bust, Back to its mansion call the fleeting breath?, Can honour's voice provoke the silent dust,, Or flattery soothe the dull cold ear of death?
""",
"""
Thomas Gray, Elegy, Stanza 11
"""),
("""
He passed the flaming bounds of place and time:, The living throne, the sapphire blaze,, Where angels tremble while they gaze,, He saw; but blasted with excess of light,, Closed his eyes in endless night.
""",
"""
Thomas Gray, Progress of Poesy, III. 2, line 99
"""),
("""
Fling but a stone, the giant dies.
""",
"""
Matthew Green, The Spleen, line 93
"""),
("""
When life is woe,, And hope is dumb,, The World says, "Go!", The Grave says, "Come!"
""",
"""
Arthur Guiterman, Betel-Nuts, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Man has the possibility of existence after death. But possibility is one thing and the realization of the possibility is quite a different thing.
""",
"""
G. I. Gurdjieff, Quotations of Gurdjieff from In Search of the Miraculous: Fragments of an Unknown Teaching (1949) by P. D. Ouspensky
"""),
("""
Death borders upon our birth; and our cradle stands in our grave.
""",
"""
Bishop Hall, Epistles, Decade III. Ep, II
"""),
("""
Come to the bridal-chamber, Death!, Come to the mother's, when she feels,, For the first time, her first-born's breath!, Come when the blessed seals, That close the pestilence are broke,, And crowded cities wail its stroke!
""",
"""
Fitz-Greene Halleck, Marco Bozzaris
"""),
("""
Ere the dolphin dies, Its hues are brightest. Like an infant's breath, Are tropic winds before the voice of death.
""",
"""
Fitz-Greene Halleck, Fortune, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
All life is surrounded by a great circumference of death; but to the believer in Jesus, beyond this surrounding death is a boundless sphere of life. He has only to die once to be done with death forever.
""",
"""
James Hamilton, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
Seek such union to the Son of God, as, leaving no present death within, shall make the second death impossible, and shall leave in all your future only that shadow of death which men call dissolution, and which the gospel calls sleeping in Jesus.
""",
"""
James Hamilton, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 181
"""),
("""
"Come and see how a Christian can die," said the dying sage to his pupil; how would it do to say, "Come and see how an infidel can die?" How would it have done for Voltaire to say this, who, in his panic at the prospect of eternity, offered his physician half his fortune for six weeks more of life?
""",
"""
James Hamilton, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 182
"""),
("""
When at last the angels come to convey your departing spirit to Abraham's bosom, depend upon it, however dazzling in their newness they may be to you, you will find that your history is no novelty, and you yourself no stranger to them.
""",
"""
James Hamilton, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 185
"""),
("""
Yes, even pricks turn into top blokes after death.
""",
"""
Andrew Hansen, "The Eulogy Song" from The Chaser's War on Everything
"""),
("""
The ancients dreaded death: the Christian can only fear dying.
""",
"""
J. C. and A. W. Hare, Guesses at Truth, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
And I hear from the outgoing ship in the bay, The song of the sailors in glee:, So I think of the luminous footprints that bore, The comfort over dark Galilee,, And wait for the signal to go to the shore,, To the ship that is waiting for me.
""",
"""
Bret Harte, The Two Ships; see also Alfred Tennyson, Crossing the Bar, Whitman
"""),
("""
Death rides on every passing breeze,, He lurks in every flower.
""",
"""
Bishop Heber, At a Funeral, Stanza 3, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Leaves have their time to fall,, And flowers to wither at the north wind's breath,, And stars to set—but all., Thou hast all seasons for thine own, O Death.
""",
"""
Felicia Hemans, Hour of Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
"Passing away" is written on the world and all the world contains.
""",
"""
Felicia Hemans, Passing Away, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Beyond this place of wrath and tears, Looms but the Horror of the shade,, And yet the menace of the years, Finds and shall find me unafraid.
""",
"""
William Ernest Henley, Invictus (1875)
"""),
("""
What is Death, But Life in act? How should the Unteeming Grave, Be victor over thee,, Mother, a mother of men?
""",
"""
W. E. Henley, Echoes, XLVI. Matri Dilectissimæ
"""),
("""
So be my passing., My task accomplished and the long day done,, My wages taken, and in my heart, Some late lark singing,, Let me be gathered to the quiet west,, The sundown splendid and serene,, Death.
""",
"""
W. E. Henley, Margaritæ Sorori
"""),
("""
Who says that we shall pass, or the fame of us fade and die,, While the living stars fulfil their round in the living sky?
""",
"""
William Ernest Henley, Rhymes And Rhythms, II
"""),
("""
Life is worth Living, Through every grain of it,, From the foundations, To the last edge, Of the cornerstone, death.
""",
"""
William Ernest Henley, Rhymes And Rhythms, XII
"""),
("""
Think on the shame of dreams for deeds,, The scandal of unnatural strife,, The slur upon immortal needs,, The treason done to life:, Arise! no more a living lie,, And with me quicken and control, Some memory that shall magnify, The universal Soul.
""",
"""
William Ernest Henley, Rhymes And Rhythms, XII
"""),
("""
Dear, was it really you and I?, In truth the riddle's ill to read,, So many are the deaths we die, Before we can be dead indeed.
""",
"""
William Ernest Henley, Rhymes And Rhythms, XV
"""),
("""
Life — give me life until the end,, That at the very top of being,, The battle-spirit shouting in my blood,, Out of the reddest hell of the fight, I may be snatched and flung, Into the everlasting lull,, The immortal, incommunicable dream.
""",
"""
William Ernest Henley, Rhymes And Rhythms, XVI
"""),
("""
From the winter’s gray despair,, From the summer’s golden languor,, Death, the lover of Life,, Frees us for ever.
""",
"""
William Ernest Henley, In Hospital (1908), page 20
"""),
("""
Not lost, but gone before.
""",
"""
Matthew Henry, Commentaries, Matthew II; itle of a song published in Smith's Edinburgh Harmony (1829)
"""),
("""
Those that God loves, do not live long.
""",
"""
George Herbert, Jacula Prudentum (1651)
"""),
("""
I know thou art gone to the home of thy rest—, Then why should my soul be so sad?, I know thou art gone where the weary are blest,, And the mourner looks up, and is glad;, I know thou hast drank of the Lethe that flows, In a land where they do not forget,, That sheds over memory only repose,, And takes from it only regret.
""",
"""
Thomas Kibble Hervey, I Know Thou Art Gone, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Upon the Grave which swallows fast/'Tis peace at last, oh peace at last.
""",
"""
Metallica, "Cyanide" (Death Magnetic), lyric by James Hetfield
"""),
("""
And death makes equal the high and low.
""",
"""
John Heywood, Be Merry Friends, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death when to death a death by death hath given, Then shall be op't the long shut gates of heaven.
""",
"""
Thomas Heywoode, Nine Bookes of various History concerning Women, Book II, of the Sybells
"""),
("""
Now I am about to take my last voyage, a great leap in the dark.
""",
"""
Thomas Hobbes. His reported last words. Hence "Hobbes' voyage," expression used by Vanbrugh in The Provoked Wife, Act V, scene 6
"""),
("""
How frighteningly few are the persons whose death would spoil our appetite and make the world seem empty.
""",
"""
Eric Hoffer, "Thoughts of Eric Hoffer, Including: ‘Absolute Faith Corrupts Absolutely,'" The New York Times Magazine (April 25, 1971), page 62
"""),
("""
When darkness gathers over all., And the last tottering pillars fall,, Take the poor dust Thy mercy warms., And mould it into heavenly forms.
""",
"""
Oliver Wendell Holmes, Sr., as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 179
"""),
("""
The mossy marbles rest, On the lips that he has pressed, In their bloom;, And the names he loved to hear, Have been carved for many a year, On the tomb.
""",
"""
Oliver Wendell Holmes, Sr., The Last Leaf, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Behold—not him we knew!, This was the prison which his soul looked through.
""",
"""
Oliver Wendell Holmes, Sr., The Last Look, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
And they die, An equal death,—the idler and the man, Of mighty deeds.
""",
"""
Homer, The Iliad, Book IX, line 396; Bryant's translation
"""),
("""
He slept an iron sleep,—, Slain fighting for his country.
""",
"""
Homer, The Iliad, Book XI, line 285; Bryant's translation
"""),
("""
It is not right to glory in the slain.
""",
"""
Homer, The Odyssey of Homer, trans. George H. Palmer (1929), book 22, line 412, page 288. Another translation is: "It isn't right to gloat over the dead." Homer's Odyssey, trans. Denison B. Hull (1978), page 252
"""),
("""
One more unfortunate, Weary of breath,, Rashly importunate,, Gone to her death!
""",
"""
Thomas Hood, Bridge of Sighs, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
We watched her breathing thro' the night,, Her breathing soft and low,, As in her breast the wave of life, Kept heaving to and fro., Our very hopes belied our fears,, Our fears our hopes belied;, We thought her dying when she slept,, And sleeping when she died.
""",
"""
Thomas Hood, The Death-bed, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
'Tis after death that we measure men.
""",
"""
James Barron Hope, "Our Heroic Dead," A Wreath of Virginia Bay Leaves, ed. Janey Hope Marr, page 71 (1895). As commander of the camp, he addressed the Confederate veterans on their first decoration day with this poem. Samuel A. Link, Pioneers of Southern Literature (1903), vol. 2, page 423
"""),
("""
Pale death, with impartial step, knocks at the hut of the poor and the towers of kings.
""",
"""
Horace, Carmina, I. 4. 13
"""),
("""
One night is awaiting us all, and the way of death must be trodden once.
""",
"""
Horace, Carmina, I. 28. 15
"""),
("""
We are all compelled to take the same road; from the urn of death, shaken for all, sooner or later the lot must come forth.
""",
"""
Horace, Carmina, II. 3. 25
"""),
("""
In the capacious urn of death, every name is shaken.
""",
"""
Horace, Carmina, III. 1. 16
"""),
("""
Swift death rushes upon us.
""",
"""
Horace, adapted from Satire 1. 8
"""),
("""
Sweet and glorious it is to die for our country.
""",
"""
Horace, Odes, book 3, ode 2, line 13. The Works of Horace, trans. J. C. Elgood (1893), page 58; there have been various translations of this sentence, including that in the Modern Library edition, The Complete Works of Horace (1936), page 217, "For country 'tis a sweet and seemly thing to die." Ernest Hemingway, in "Notes on the Next War," Esquire (September 1935), page 156, said, "They wrote in the old days that it is sweet and fitting to die for one's country. But in modern war there is nothing sweet nor fitting in your dying."
"""),
("""
In the democracy of the dead all men at last are equal. There is neither rank nor station nor prerogative in the republic of the grave.
""",
"""
John James Ingalls, eulogy on the death of Representative James N. Burnes, January 24, 1889, reported in A Collection of the Writings of John James Ingalls (1902), page 273
"""),
("""
We all do fade as a leaf.
""",
"""
Isaiah. LXIV. 6
"""),
("""
Death's got to be easy, because life is hard. It'll leave you physically, mentally, and emotionally scarred.
""",
"""
Curtis J. Jackson, "Many Men" (2003), Get Rich or Die Tryin'
"""),
("""
You're on your way to meet your maker... Are you ready? No exceptions to the rule; death is promised.
""",
"""
Curtis J. Jackson, "Gunz Come Out" (2005), The Massacre
"""),
("""
And those slain by Jehovah in that day will be from one end of the earth clear to the other end of the earth. They will not be mourned, nor will they be gathered up or buried. They will become like manure on the surface of the ground.
""",
"""
Jeremiah 25:33
"""),
("""
The Lord gave, and the Lord hath taken away; blessed be the name of the Lord.
""",
"""
Job. I. 21
"""),
("""
He shall return no more to his house, neither shall his place know him any more.
""",
"""
Job, VII. 10
"""),
("""
The land of darkness and the shadow of death.
""",
"""
Job. X. 21
"""),
("""
And fear not them which kill the body, but are not able to kill the soul: but rather fear him which is able to destroy both soul and body in Hell.
""",
"""
Jesus, in Matthew 10:28
"""),
("""
I saw him, I fell at his feet as dead. And he laid his right hand upon me, saying unto me, Fear not; I am the first and the last:  I am he that liveth, and was dead; and, behold, I am alive for evermore, Amen; and have the keys of hell and of death.
""",
"""
John of Patmos Revelation 6:7-8
"""),
("""
I am not afraid to kill you for their is no death.
""",
"""
Alejandro's Jodorowsky, El Topo
"""),
("""
Then with no fiery throbbing pain,, No cold gradations of decay,, Death broke at once the vital chain,, And freed his soul the nearest way.
""",
"""
Samuel Johnson, Verses on the Death of Mr. Robert Levet, Stanza 9. ("No fiery throbs of pain" in first edition)
"""),
("""
Depend upon it, Sir, when a man knows he is to be hanged in a fortnight, it concentrates his mind wonderfully.
""",
"""
Samuel Johnson, reported in James Boswell, Boswell's Life of Johnson, ed. George B. Hill, rev. and enl. ed., ed. L. F. Powell (1934), entry for September 19, 1777, vol. 3, page 167
"""),
("""
Disease generally begins that equality which death completes.
""",
"""
Samuel Johnson, The Rambler, vol. 1 (1793), page 110
"""),
("""
Thou art but gone before,, Whither the world must follow.
""",
"""
Ben Jonson, Epitaph on Sir John Roe, in Dodd's Epigrammatists, page 190
"""),
("""
Death alone discloses how insignificant are the puny bodies of men.
""",
"""
Juvenal, Satires, X. 172
"""),
("""
Trust to a plank, draw precarious breath,, At most seven inches from the jaws of death.
""",
"""
Juvenal, Satires, XII. 57. Gifford's translation
"""),
("""
Verse, Fame and beauty are intense indeed,, But Death intenser – Death is life's high mead.
""",
"""
John Keats, Sonnet: Why did I laugh to-night?
"""),
("""
Love masters agony; the soul that seemed, Forsaken feels her present God again, And in her Father's arms, Contented dies away.
""",
"""
John Keble, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 182
"""),
("""
No one can obtain from the Pope a dispensation for never dying.
""",
"""
Thomas à Kempis, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Dying is not a crime.
""",
"""
Jack Kevorkian, as quoted in Introducing Christian Ethics (2010), by Samuel Wells and Ben Quash, John Wiley and Sons, page 329
"""),
("""
Despite the solace of hypocritical religiosity and its seductive promise of an after-life of heavenly bliss, most of us will do anything to thwart the inevitable victory of biological death.
""",
"""
Jack Kevorkian, describing his painting "Nearer My God To Thee", as quoted in "I Can Think of Life, and Nothing Else" by Cliff Walker (January 2002)
"""),
("""
When you and I behind the Veil are past.
""",
"""
Omar Khayyam, Rubaiyat of Omar Khayyam (1120), Stanza 47. (Not in first edition); FitzGerald's translation
"""),
("""
Strange—is it not?—that of the myriads who, Before us passed the door of Darkness through,, Not one returns to tell us of the road, Which to discover we must travel too.
""",
"""
Omar Khayyam, Rubaiyat of Omar Khayyam (1120), Stanza 68. FitzGerald's translation
"""),
("""
If being a kid is about learning how to live, then being a grown-up is about learning how to die.
""",
"""
Narration; Stephen King, Christine, Part 1, Chapter 5
"""),
("""
Nay, why should I fear Death,, Who gives us life, and in exchange takes breath?
""",
"""
Frederic L. Knowles, Laus Mortis, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
When I have folded up this tent, And laid the soiled thing by,, I shall go forth 'neath different stars,, Under an unknown sky.
""",
"""
Frederic L. Knowles, The Last Word, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The thunderstorm is a constant phenomenon, raging alternately over some part of the world or the other. Can a single man or creature escape death if all that charge of lightning strikes the earth?
""",
"""
Kalki Krishnamurthy, in "Sivakozhundu of Tiruvazhundur" as translated in Kalki: Selected Stories (1999)
"""),
("""
Gone before, To that unknown and silent shore.
""",
"""
Charles Lamb, Hester, Stanza 1, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
One destined period men in common have,, The great, the base, the coward, and the brave,, All food alike for worms, companions in the grave.
""",
"""
Lord Lansdowne, Meditation on Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The first day after a death, the new absence, Is always the same; we should be careful, Of each other, we should be kind, While there is still time.
""",
"""
Philip Larkin, "The Mower" (1979)
"""),
("""
We think each one will heave to and unload
""",
"""
Philip Larkin, “Next, Please"
"""),
("""
Neither the sun nor death can be looked at with a steady eye.
""",
"""
François de La Rochefoucauld, Maxims, 36
"""),
("""
'What you love, you will love. What you undertake you will complete. You are a fulfiller of hope; you are to be relied on. But seventeen years give little armor against despair...Consider, Arren. To refuse death is to refuse life.
""",
"""
Ursula K. Le Guin, The Farthest Shore (1972), Chapter 8, "The Children of the Open Sea" (Ged)
"""),
("""
O Time! consumer of all things; O envious age! thou dost destroy all things and devour all things with the relentless teeth of years, little by little in a slow death. Helen, when she looked in her mirror, seeing the withered wrinkles made in her face by old age, wept and wondered why she had twice been carried away.
""",
"""
Leonardo da Vinci, The Notebooks of Leonardo Da Vinci (1938), XIX Philosophical Maxims. Morals. Polemics and Speculations., as translated by Edward MacCurdy
"""),
("""
And, as she looked around, she saw how Death, the consoler,, Laying his hand upon many a heart, had healed it forever.
""",
"""
Henry Wadsworth Longfellow, Evangeline: A Tale of Acadie (1847), Part II. V
"""),
("""
The young may die, but the old must!
""",
"""
Henry Wadsworth Longfellow, Christus, The Golden Legend (1872), Part IV. The Cloisters
"""),
("""
There is no confessor like unto Death!, Thou canst not see him, but he is near:, Thou needest not whisper above thy breath,, And he will hear;, He will answer the questions,, The vague surmises and suggestions,, That fill thy soul with doubt and fear.
""",
"""
Henry Wadsworth Longfellow, Christus, The Golden Legend (1872), Part V. The Inn at Genoa
"""),
("""
Death never takes one alone, but two!, Whenever he enters in at a door,, Under roof of gold or roof of thatch,, He always leaves it upon the latch,, And comes again ere the year is over,, Never one of a household only.
""",
"""
Henry Wadsworth Longfellow, Christus, The Golden Legend (1872), Part VI. The Farm-House in the Odenwald
"""),
("""
There is a Reaper whose name is Death,, And with his sickle keen,, He reaps the bearded grain at a breath,, And the flowers that grow between.
""",
"""
Henry Wadsworth Longfellow, Reaper and the Flowers. Compare Arnim and Brentano—Erntelied, in Des Knaben Wunderhorn. (Ed. 1857), Volume I, page 59
"""),
("""
There is no Death! What seems so is transition;, This life of mortal breath, Is but a suburb of the life elysian,, Whose portal we call Death.
""",
"""
Henry Wadsworth Longfellow, Resignation, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
There is no flock, however watched and tended,, But one dead lamb is there!, There is no fireside howsoever defended,, But has one vacant chair.
""",
"""
Henry Wadsworth Longfellow, Resignation, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Oh, what hadst thou to do with cruel Death,, Who wast so full of life, or Death with thee,, That thou shouldst die before thou hadst grown old!
""",
"""
Henry Wadsworth Longfellow, Three Friends of Mine, Part II, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Then fell upon the house a sudden gloom,, A shadow on those features fair and thin;, And softly, from the hushed and darkened room,, Two angels issued, where but one went in.
""",
"""
Henry Wadsworth Longfellow, Two Angels, Stanza 9
"""),
("""
Were a star quenched on high,, For ages would its light,, Still travelling downward from the sky,, Shine on our mortal sight., So when a great man dies,, For years beyond our ken,, The light he leaves behind him lies, Upon the paths of men.
""",
"""
Henry Wadsworth Longfellow, "Charles Sumner", stanzas 8 and 9, The Poetical Works of Longfellow (1893, reprinted 1975), page 324
"""),
("""
I imagined it was more difficult to die.
""",
"""
Louis XIV, To Madame de Maintenon. See Martin, History of France, XIV, Book XCI
"""),
("""
That is not dead which can eternal lie / And with strange aeons even Death may die.
""",
"""
H.P. Lovecraft, in "The Call of Cthulhu"
"""),
("""
But life is sweet, though all that makes it sweet, Lessen like sound of friends' departing feet;, And Death is beautiful as feet of friend, Coming with welcome at our journey's end.
""",
"""
James Russell Lowell, An Epistle to George William Curtis, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The gods conceal from those destined to live how sweet it is to die, that they may continue living.
""",
"""
Marcus Annaeus Lucanus, Pharsalia, IV. 519
"""),
("""
Death is free from the restraint of Fortune; the earth takes everything which it has brought forth.
""",
"""
Marcus Annaeus Lucanus, Pharsalia, VII. 818
"""),
("""
The coward and the courageous alike must die.
""",
"""
Marcus Annaeus Lucanus, Pharsalia, IX. 582
"""),
("""
From the very jaws of death I have escaped to this condition.
""",
"""
Lucretius, Appage Met, VII, page 191
"""),
("""
Nay, the greatest wits and poets, too, cease to live;  Homer, their prince, sleeps now in the same forgotten sleep as do the others.
""",
"""
Lucretius, De Rerum Natura, III. 1,049
"""),
("""
The axe is laid unto the root of the trees.
""",
"""
Luke, III. 9
"""),
("""
What is our death but a night's sleep? For as through sleep all weariness and faintness pass away and cease, and the powers of the spirit come back again, so that in the morning we arise fresh and strong and joyous; so at the Last Day we shall rise again as if we had only slept a night, and shall be fresh and strong.
""",
"""
Martin Luther, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 178
"""),
("""
To every man upon this earth, Death cometh soon or late,, And how can man die better, Than facing fearful odds,, For the ashes of his fathers, And the temples of his gods?
""",
"""
Thomas Babington Macaulay, 1st Baron Macaulay, Lays of Ancient Rome, Horatius, XXVII
"""),
("""
There is no such thing as death., In nature nothing dies., From each sad remnant of decay, Some forms of life arise.
""",
"""
Charles Mackay, There is No Such Thing as Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
If life has not made youby God's grace, through faith, holy — think you, will death, without faith do it? The cold waters of that narrow stream are no purifying bath in which you may wash and be clean. No! no! as you go down into them, you will come up from them.
""",
"""
Alexander Maclaren, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
It is an ideal for which I am prepared to die.
""",
"""
Nelson Mandela, I am prepared to die statement in the Rivonia trial, Pretoria Supreme court (20 April 1964)
"""),
("""
Death is something inevitable. When a man has done what he considers to be his duty to his people and his country, he can rest in peace. I believe I have made that effort and that is, therefore, why I will sleep for the eternity.
""",
"""
Nelson Mandela, from a interview for the documentary Mandela (1994); also in From Nelson Mandela By Himself: The Authorised Book of Quotations © 2010 by Nelson R. Mandela and The Nelson Mandela Foundation
"""),
("""
All our knowledge merely helps us to die a more painful death than the animals that know nothing.
""",
"""
Manilius. Joyzelle, Act I
"""),
("""
We begin to die as soon as we are born, and the end is linked to the beginning.
""",
"""
Manilius, Astronomica, IV. 16
"""),
("""
I want to meet my God awake.
""",
"""
Maria-Theresa, who refused to take a drug when dying, according to Thomas Carlyle
"""),
("""
This I ask, is it not madness to kill thyself in order to escape death?
""",
"""
Martial, Epigrams (c. 80-104 AD), II. 80. 2
"""),
("""
When the last sea is sailed and the last shallow charted,, When the last field is reaped and the last harvest stored,, When the last fire is out and the last guest departed, Grant the last prayer that I shall pray, Be good to me, O Lord.
""",
"""
John Masefield, D'Avalos' Prayer, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
When Life knocks at the door no one can wait,, When Death makes his arrest we have to go.
""",
"""
John Masefield, Widow in the Bye Street, Part II
"""),
("""
Death opens unknown doors. It is most grand to die.
""",
"""
John Masefield, Pompey the Great, I
"""),
("""
She thought our good-night kiss was given,, And like a lily her life did close;, Angels uncurtained that repose,, And the next waking dawned in heaven.
""",
"""
Gerald Massey, The Ballad of Babe Christabel, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death hath a thousand doors to let out life., I shall find one.
""",
"""
Philip Massinger, A Very Woman, Act V, scene 4
"""),
("""
In this world, one day death is going to take the life from everything that you love. So while you're able, love what you have. Takes the death from your life.
""",
"""
Mercy Ealing to Joe Carpenter, from Sole Survivor (2000 film), teleplay by Richard Christian Matheson
"""),
("""
Death is for the living and not for the dead.
""",
"""
Floyd McClure in Gates of Heaven (1980)
"""),
("""
There is no death! the stars go down, To rise upon some other shore,, And bright in Heaven's jeweled crown,, They shine for ever more.
""",
"""
John L. McCreery, Arthur's Home Magazine (July, 1863), Volume 22, page 41. Wrongly ascribed to Bulwer-Lytton
"""),
("""
At the end of your life, you're lucky if you die.
""",
"""
Bret McKenzie, "Think About It Think Think About It" - Flight of the Conchords
"""),
("""
O that we may all be living in such a state of preparedness, that, when summoned to depart, we may ascend the summit whence faith looks forth on all that Jesus hath suffered and done, and exclaiming, " We have waited for Thy salvation, O Lord," lie down with Moses on Pisgah, to awake with Moses in paradise.
""",
"""
Henry Melvill, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 181
"""),
("""
He whom the gods love dies young.
""",
"""
Menander, Dis Exapaton; also in Dionysius, Ars Rhetorica, Volume V, page 364, Reiske's Edition
"""),
("""
There's nothing certain in man's life but this:, That he must lose it.
""",
"""
Owen Meredith (Lord Lytton), Clytemnestra, Part XX
"""),
("""
If I should die to-night,, My friends would look upon my quiet face, Before they laid it in its resting-place,, And deem that death had left it almost fair.
""",
"""
Robert C. V. Meyers, If I should Die Tonight, in 100 Choice Selections, No. 27, page 172
"""),
("""
Death did not come to my mother, Like an old friend., She was a mother, and she must, Conceive him., Up and down the bed she fought crying, Help me, but death, Was a slow child, Heavy.
""",
"""
Josephine Miles, "Conception" (1974) st. 1–2; Collected Poems (1983)
"""),
("""
Today if death did not exist, it would be necessary to invent it.
""",
"""
Millaud, when voting for the death of Louis XVI; Otto von Bismarck used same expression to Chevalier Nigra, referring to Italy, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death is delightful. Death is dawn,, The waking from a weary night, Of fevers unto truth and light.
""",
"""
Joaquin Miller, Even So, Stanza 35
"""),
("""
Death cannot come To him untimely who is fit to die; The less of this cold world, the more of heaven; The briefer life, the earlier immortality.
""",
"""
Henry Hart Milman, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
O fairest flower; no sooner blown but blasted,, Soft, silken primrose fading timelessly.
""",
"""
John Milton, Ode on the Death of a Fair Infant Dying of a Cough, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
So spake the grisly Terror.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book II, line 704
"""),
("""
I fled, and cried out Death;, Hell trembled at the hideous name, and sighed, From all her caves, and back resounded Death.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book II, line 787
"""),
("""
Before mine eyes in opposition sits, Grim Death, my son and foe.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book II, line 803
"""),
("""
Death, Grinned horrible a ghastly smile, to hear, His famine should be filled.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book II, line 845
"""),
("""
Eased the putting off, These troublesome disguises which we wear.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book IV, line 739
"""),
("""
Behind her Death, Close following pace for pace, not mounted yet, On his pale horse.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book X, line 588
"""),
("""
How gladly would I meet, Mortality my sentence, and be earth, Insensible! how glad would lay me down, As in my mother's lap!
""",
"""
John Milton, Paradise Lost (1667; 1674), Book X, line 775
"""),
("""
And over them triumphant Death his dart, Shook, but delayed to strike, though oft invoked.
""",
"""
John Milton, Paradise Lost (1667; 1674), Book XI, line 491
"""),
("""
We are all mortal, and each one is for himself.
""",
"""
Molière, L'École des Femmes, II, 6
"""),
("""
Rome can give no dispensation from death.
""",
"""
Molière, L'Etourdi, II, 4
"""),
("""
Philosophy first commands us to have death ever before our eyes, to anticipate it and consider it beforehand, and then she gives us rules and caveats in order to forestall our being hurt by our reflections!
""",
"""
Montaigne, Essays, M. Screech, trans. (1991), Book III, Chapter 12, “Of Physiognomy,” page1190
"""),
("""
Death, they say, acquits us of all obligations.
""",
"""
Michel de Montaigne, Essays, Book I, Chapter 7; also La mort est la recepte a touts maulx Montaigne—Essays, Book II, Chapter III
"""),
("""
There's nothing terrible in death;, 'Tis but to cast our robes away,, And sleep at night, without a breath, To break repose till dawn of day.
""",
"""
James Montgomery, In Memory of E. G, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Thus star by star declines, Till all are passed away,, As morning high and higher shines, To pure and perfect day:, Nor sink those stars in empty night;, They hide themselves in heaven's pure light.
""",
"""
James Montgomery, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
Weep not for those whom the veil of the tomb, In life's happy morning hath hid from our eyes,, Ere sin threw a blight over the spirit's young bloom, Or earth had profaned what was born for the skies.
""",
"""
Thomas Moore, Song, Weep not for Those, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
How short is human life! the very breath, Which frames my words accelerates my death.
""",
"""
Hannah More, King Hezekiah, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
At end of Love, at end of Life,, At end of Hope, at end of Strife,, At end of all we cling to so—, The sun is setting—must we go?, , At dawn of Love, at dawn of Life,, At dawn of Peace that follows Strife,, At dawn of all we long for so—, The sun is rising—let us go.
""",
"""
Louise Chandler Moulton, At End, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
There is rust upon locks and hinges,, And mould and blight on the walls,, And silence faints in the chambers,, And darkness waits in the halls.
""",
"""
Louise Chandler Moulton, House of Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Yes, death, — the hourly possibility of it, — death is the sublimity of life.
""",
"""
William Mountford, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
Death is not the opposite of life; it exists as a part of them.
""",
"""
Haruki Murakami, in Norwegian Wood (1987)
"""),
("""
Thal leader: There is no indignity in being afraid to die, but there is a terrible shame in being afraid to live.
""",
"""
Terry Nation The Daleks Doctor Who
"""),
("""
Exterminate!
""",
"""
Terry Nation, in the identifying statement of "The Daleks", an episode of Doctor Who (1963)
"""),
("""
All humans will, without exception, eventually die. After they die, the place they go is Mu (nothingness).
""",
"""
Death Note vol. 12 pg. 188 by Tsugumi Ohba
"""),
("""
I don't know what God is., I don't know what death is., But I believe they have between them, some fervent and necessary arrangement.
""",
"""
Mary Oliver, Red Bird (2008), "Sometimes", § 1
"""),
("""
And die with decency.
""",
"""
Thomas Otway, Venice Preserved, Act V, scene 3
"""),
("""
We are all bound thither; we are hastening to the same common goal. Black death calls all things under the sway of its laws.
""",
"""
Ovid, Ad Liviam, 359
"""),
("""
Thou fool, what is sleep but the image of death? Fate will give an eternal rest.
""",
"""
Ovid, Amorum (16 BC), II. 9. 41
"""),
("""
Man should ever look to his last day, and no one should be called happy before his funeral.
""",
"""
Ovid, Metamorphoses, III. 135
"""),
("""
Death is not grievous to me, for I shall lay aside my pains by death.
""",
"""
Ovid, Metamorphoses, III. 471
"""),
("""
Wherever you look there is nothing but the image of death.
""",
"""
Ovid, Tristium, I. 2. 23
"""),
("""
I don't care when I die, when you die, you die. What I don't want is to die in my bed. To be killed in an accident or to be shot is my preferred way to die.
""",
"""
Ashraf Pahlavi, In Bitter American Exile, the Shah's Twin Sister, Ashraf, Defends Their Dynasty, People (May 05, 1980)
"""),
("""
Death's but a path that must be trod,, If man would ever pass to God.
""",
"""
Thomas Parnell, Night-Piece on Death, line 67
"""),
("""
Reflect on death as in Jesus Christ, not as without Jesus Christ. Without Jesus Christ it is dreadful, it is alarming, it is the terror of nature. In Jesus Christ it is fair and lovely, it is good and holy, it is the joy of saints.
""",
"""
Blaise Pascal, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 176
"""),
("""
For he must rule as king until God has put all enemies under his feet. And the last enemy, death, is to be brought to nothing.
""",
"""
Paul of Tarsus, 1 Corinthians 15: 25-26, NWT
"""),
("""
Death is swallowed up in victory. O Death where is thy sting, O Grave where is thy victory?
""",
"""
Paul of Tarsus, I Corinthians 15:54 - 56
"""),
("""
Death is repose, but the thought of death disturbs all repose.
""",
"""
Cesare Pavese, This Business of Living, 1938-06-07
"""),
("""
Death comes to all. His cold and sapless hand, Waves over the world, and beckons us away., Who shall resist the summons?
""",
"""
Thomas Love Peacock, Time
"""),
("""
O lady, he is dead and gone!, Lady, he's dead and gone!, And at his head a green grass turfe,, And at his heels a stone.
""",
"""
Thomas Percy, Reliques. The Friar of Orders Gray
"""),
("""
For death betimes is comfort, not dismay,, And who can rightly die needs no delay.
""",
"""
Petrarch, To Laura in Death. Canzone V, Stanza 6
"""),
("""
Earl of Sandwich: 'Pon my honor, Wilkes, I don't know whether you'll die on the gallows or of the pox., John Wilkes: That must depend my Lord, upon whether I first embrace your Lordship's principles, or your Lordship's mistresses.
""",
"""
Exchange retold by Sir Charles Petrie, in The Four Georges, page 133 (1935)
"""),
("""
For life is nearer every day to death.
""",
"""
Phaedrus, Fables, Book IV. 25. 10
"""),
("""
Look forward a little further to the period when all the noise and tumult and business of this world shall have closed forever.
""",
"""
John Gregory Pike, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 174
"""),
("""
He whom the gods love dies young, whilst he is full of health, perception, and judgment.
""",
"""
Plautus, Bacchides, Act IV. 7. 18
"""),
("""
His last day places man in the same state as he was before he was born; nor after death has the body or soul any more feeling than they had before birth.
""",
"""
Pliny the Elder, Historia Naturalis, LVI. 1
"""),
("""
Concerning the dead nothing but good shall be spoken.
""",
"""
Plutarch, Life of Solon. Given as a saying of Solon. Attributed also to Chilo
"""),
("""
Come! let the burial rite be read—, The funeral song be sung!—, An anthem for the queenliest dead, That ever died so young—, A dirge for her, the doubly dead, In that she died so young.
""",
"""
Edgar Allen Poe, Lenore, Stanza 1
"""),
("""
Out—out are the lights—out all!, And, over each quivering form,, The curtain, a funeral pall,, Comes down with the rush of a storm,, And the angels, all pallid and wan,, Uprising, unveiling, affirm, That the play is the tragedy, "Man,", And its hero the Conqueror Worm.
""",
"""
Edgar Allen Poe, The Conqueror Worm, Stanza 5
"""),
("""
See my lips tremble and my eyeballs roll,, Suck my last breath, and catch my flying soul!
""",
"""
Alexander Pope, Eloisa to Abelard (1717), line 323
"""),
("""
O Death, all eloquent! you only prove, What dust we dote on, when 'tis man we love.
""",
"""
Alexander Pope, Eloisa to Abelard (1717), line 355
"""),
("""
Till tired, he sleeps, and life's poor play is over.
""",
"""
Alexander Pope, An Essay on Man (1733-34), Epistle II, line 282
"""),
("""
But thousands die without or this or that,, Die, and endow a college or a cat.
""",
"""
Alexander Pope, Moral Essays (1731-35), Epistle III, line 95
"""),
("""
Tell me, my soul! can this be death?
""",
"""
Alexander Pope, The Dying Christian to His Soul; Pope attributes his inspiration to Hadrian and to a Fragment of Sappho, as reported in Croly's edition of Pope (1835). Thomas Flatman, used a similar paraphrase before Pope was born in Thoughts on Death (1674).
"""),
("""
The world recedes; it disappears;, Heav'n opens on my eyes; my ears, With sounds seraphic ring:, Lend, lend your wings! I mount! I fly!, O Grave! where is thy victory?, O Death! where is thy sting?
""",
"""
Alexander Pope, The Dying Christian to His Soul
"""),
("""
Vital spark of heavenly flame!, Quit, oh quit this mortal frame.
""",
"""
Alexander Pope, The Dying Christian to His Soul
"""),
("""
By foreign hands thy dying eyes were closed,, By foreign hands thy decent limbs composed,, By foreign hands thy humble grave adorned,, By strangers honoured, and by strangers mourned.
""",
"""
Alexander Pope, Elegy to the Memory of an Unfortunate Lady, line 51
"""),
("""
A heap of dust remains of thee;, 'Tis all thou art, and all the proud shall be!
""",
"""
Alexander Pope, Elegy to the Memory of an Unfortunate Lady, line 73
"""),
("""
It was said that life was cheap in Ankh-Morpork. This was, of course, completely wrong. Life was often very expensive; you could get death for free.
""",
"""
Terry Pratchett in Pyramids (1989)
"""),
("""
I am death, not taxes. I turn up only once.
""",
"""
Terry Pratchett, Feet of Clay (1996)
"""),
("""
Teach him how to live,, And, oh! still harder lesson! how to die.
""",
"""
Beilby Porteus, Death, line 316
"""),
("""
There is something beyond the grave; death does not end all, and the pale ghost escapes from the vanquished pyre., Propertius Elegies IV, vii, 1.
""",
"""
Proverbs, VI. 10; XXIV. 33
"""),
("""
Yet a little sleep, a little slumber, a little folding of the hands to sleepage
""",
"""
Proverbs, VI. 10; XXIV. 33
"""),
("""
There is a way which seemeth right unto a man, but the end thereof are the ways of death.
""",
"""
Proverbs 14:12 (KJV)
"""),
("""
I have said ye are gods … But ye shall die like men.
""",
"""
Psalms. LXXXII. 6. 7
"""),
("""
Death aims with fouler spite, At fairer marks.
""",
"""
Francis Quarles, Divine Poems (Ed. 1669)
"""),
("""
It is the lot of man but once to die.
""",
"""
Francis Quarles, Emblems, Book V, Emblem 7
"""),
("""
Withdrawn into the peace of this desert,, along with some books, few but wise,, I live in conversation with the deceased,, and listen to the dead with my eyes.
""",
"""
Francisco de Quevedo, From the Tower
"""),
("""
I am going to seek a great perhaps; draw the curtain, the farce is played.
""",
"""
Attributed to Rabelais by tradition; in Motteux's Life of Rabelais this is paraphrased: "I am about to leap into the dark"; also Notice sur Rabelais in Œuvres de F. Rabelais. Paris, 1837, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
And greedy Acheron does not relinquish its prey.
""",
"""
Jean Racine, Phèdre, Act II, scene 5
"""),
("""
Hushed in the alabaster arms of Death,, Our young Marcellus sleeps.
""",
"""
James R. Randall, John Pelham, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death is always pointless... That is the point.
""",
"""
Michael Reaves & Brynne Chandler Reaves, in "Grief", episode of Gargoyles
"""),
("""
The long sleep of death closes our scars, and the short sleep of life our wounds.
""",
"""
Jean Paul Richter, Hesperus, XX
"""),
("""
But death we are, and death have always been.
""",
"""
Anne Rice, Interview with the Vampire (film), 1994, 1h16m14s
"""),
("""
The imminence of death serves to sweep away the inessential preoccupations for those who do not flee from the thought of death into triviality., , David Riesman, “Clinical and Cultural Aspects of the Aging Process,” Individualism Reconsidered (1954), page 485
""",
"""
Jane Roberts, The Seth Material (1970), page 123
"""),
("""
Those that he loved so long and sees no more,, Loved and still loves—not dead, but gone before,, He gathers round him.
""",
"""
Samuel Rogers, Human Life, line 739
"""),
("""
Mortal danger is an effective antidote for fixed ideas.
""",
"""
Erwin Rommel, as quoted in The Rommel Papers (1982) edited by Basil Henry Liddell Hart
"""),
("""
Sleep that no pain shall wake,, Night that no morn shall break,, Till joy shall overtake, Her perfect peace.
""",
"""
Christina G. Rossetti, Dream-Land, Stanza 4
"""),
("""
There is no music more for him:, His lights are out, his feast is done;, His bowl that sparkled to the brim, Is drained, is broken, cannot hold.
""",
"""
Christina G. Rossetti, Peal of Bells
"""),
("""
When I am dead, my dearest,, Sing no sad songs for me;, Plant thou no roses at my head,, No shady cypress tree.
""",
"""
Christina G. Rossetti, Song, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
I go to see the sun for the last time.
""",
"""
Rousseau's last words, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death is the privilege of human nature,, And life without it were not worth our taking:, Thither the poor, the pris'ner, and the mourner, Fly for relief, and lay their burthens down.
""",
"""
Nicholas Rowe, The Fair Penitent (1703), Act V, scene 1, line 138
"""),
("""
To fear love is to fear life, and those who fear life are already three parts dead.
""",
"""
Bertrand Russell, Marriage and Morals (1929),Ch. 19: Sex and Individual Well-Being
"""),
("""
Out of the chill and the shadow,, Into the thrill and the shine;, Out of the dearth and the famine,, Into the fulness divine.
""",
"""
Margaret E. Sangster, Going Home, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death must be an evil — and the gods agree;, for why else would they live for ever?
""",
"""
Sappho (c. 600 B.C.). Poetarum Lesbiorum Fragmenta, ed. Edgar Lobel and Denys Page. Oxford: Clarendon Press, 1955, no. 201
"""),
("""
Death is the inventor of God.
""",
"""
José Saramago, in an interview with "El País", 2009
"""),
("""
One day in the afternoon of the world, glum death will come and sit in you, and when you get up to walk, you will be as glum as death, but if you're lucky, this will only make the fun better and the love greater.
""",
"""
William Saroyan, "One Day in the Afternoon of the World" (1964)
"""),
("""
We may have years, we may have hours, but sooner or later, we push up flowers.
""",
"""
Tim Schafer, Grim Fandango
"""),
("""
Death makes sad stories of us all.
""",
"""
Tim Schafer, Grim Fandango
"""),
("""
I hope that Allah will not make me immortal, for death is his greatest gift to any true believer.
""",
"""
Scheherazade One Thousand and One Nights Tale of King Umar al-Numan, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Day's lustrous eyes grow heavy in sweet death.
""",
"""
Friedrich Schiller, Assignation, Stanza 4. Lord Lytton's translation
"""),
("""
If you do not dare to die you will never win life.
""",
"""
Friedrich Schiller, Wallenstein's Lager, XI. Chorus
"""),
("""
Good night, Gordon. I am thinking of taking a long sleepage
""",
"""
Friedrich Schiller, Wallenstein's Tod, V. 5. 85
"""),
("""
Each day is a little life: every waking and rising a little birth, every fresh morning a little youth, every going to rest and sleep a little death.
""",
"""
Arthur Schopenhauer, Counsels and Maxims, T. B. Saunders, trans., § 13
"""),
("""
Haste thee, haste thee, to be gone!, Earth flits fast and time draws on:, Gasp thy gasp, and groan thy groan!, Day is near the breaking.
""",
"""
Walter Scott, Death Chant, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Soon the shroud shall lap thee fast,, And the sleep be on thee cast, That shall never know waking.
""",
"""
Walter Scott, Guy Mannering, Chapter XXVII
"""),
("""
Like the dew on the mountain,, Like the foam on the river,, Like the bubble on the fountain,, Thou art gone, and for ever!
""",
"""
Walter Scott, Lady of the Lake (1810), Canto III, Stanza 16
"""),
("""
Death ends a life, not a relationshipage
""",
"""
Morrie Schwartz, as quoted from Tuesdays with Morrie (1997)
"""),
("""
Death is a stage in human progress, to be passed as we would pass from childhood to youth, or from youth to manhood, and with the same consciousness of an everlasting nature.
""",
"""
Edmund Sears, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 177
"""),
("""
So die as though your funeral, Ushered you through the doors that led, Into a stately banquet hall, Where heroes banqueted.
""",
"""
Alan Seeger, Maktoob, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
What new thing then is it for a man to die, whose whole life is nothing else but a journey to death?
""",
"""
Seneca the Younger, De Consol. ad Polyb. 30
"""),
("""
It is an extreme evil to depart from the company of the living before you die.
""",
"""
Seneca the Younger, De Tranquilitate Animi. 2
"""),
("""
They will not live, and do not know how to die.
""",
"""
Seneca the Younger, Epistles, IV
"""),
("""
They are not lost but sent before.
""",
"""
Seneca the Younger, Epistles, LXIII. 16. Early sources in Cyprian—De Mortalitate. S, XX
"""),
("""
It is folly to die of the fear of death.
""",
"""
Seneca the Younger, Epistles, LXIX
"""),
("""
It is uncertain in what place death may await thee; therefore expect it in any place.
""",
"""
Seneca the Younger, Epistolæ Ad Lucilium, XXVI
"""),
("""
This day, which thou fearest as thy last, is the birthday of eternity.
""",
"""
Seneca the Younger, Epistolæ Ad Lucilium, CII
"""),
("""
Sometimes death is a punishment; often a gift; it has been a favor to many.
""",
"""
Seneca the Younger, Hercules Oetæus, CMXXX
"""),
("""
Any one may take life from man, but no one death; a thousand gates stand open to it.
""",
"""
Seneca the Younger, Phœnissæ, CLII
"""),
("""
To die without fear of death is to be desired.
""",
"""
Seneca the Younger, Troades, DCCCLXIX
"""),
("""
On him does death lie heavily, who, but too well known to all, dies to himself unknown.
""",
"""
Seneca the Younger, Thyestes, lines 401-403 (Chorus)
"""),
("""
Mr. Bookman: Mr. Death, that little girl's only 8 years old. I'm ready now.
""",
"""
Rod Serling, in Twilight Zone episode "One for the Angels"
"""),
("""
Death's pale flag advanced in his cheeks.
""",
"""
Seven Champions, Part III, Chapter XI
"""),
("""
Cowards die many times before their deaths;
""",
"""
William Shakespeare, Julius Caesar, Act 2 scene 2
"""),
("""
To-morrow, and to-morrow, and to-morrow,
""",
"""
William Shakespeare, Macbeth, Act 5 scene 5
"""),
("""
The babe is at peace within the womb,, The corpse is at rest within the tomb., We begin in what we end.
""",
"""
Percy Bysshe Shelley, Fragments. Same idea in Thomas Browne, Hydriotaphia, page 221. (St. John's ed.)
"""),
("""
First our pleasures die—and then, Our hopes, and then our fears—and when, These are dead, the debt is due,, Dust claims dust—and we die too.
""",
"""
Percy Bysshe Shelley, Death (1820)
"""),
("""
All buildings are but monuments of death,, All clothes but winding-sheets for our last knell,, All dainty fattings for the worms beneath,, All curious music but our passing bell:, Thus death is nobly waited on, for why?, All that we have is but death's livery.
""",
"""
James Shirley, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death calls ye to the crowd of common men.
""",
"""
James Shirley, Cupid and Death, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The glories of our blood and state, Are shadows, not substantial things;, There is no armour against fate,, Death lays his icy hand on kings., Scepter and crown, Must tumble down,, And, in the dust, be equal made, With the poor crooked scythe and spade.
""",
"""
James Shirley, Contention of Ajax and Ulysses, scene 3. ("Birth and State" in Percy's Reliques. These lines are said to have terrified Cromwell), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He that on his pillow lies,, Fear-embalmed before he dies, Carries, like a sheep, his life,, To meet the sacrificer's knife,, And for eternity is prest,, Sad bell-wether to the rest.
""",
"""
James Shirley, The Passing Bell, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death without phrases.
""",
"""
Emmanuel-Joseph Sieyès, voting for the death of Louis XVI. (Denied by him). He no doubt voted "La mort"; "sans phrase" being a note on the laconic nature of his vote, i.e. without remarks. The voting usually included explanations of the decision; as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Yet 'twill only be a sleep:, When, with songs and dewy light,, Morning blossoms out of Night,, She will open her blue eyes, 'Neath the palms of Paradise,, While we foolish ones shall weepage
""",
"""
Edward Rowland Sill, Sleeping, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
We count it death to falter, not to die.
""",
"""
Simonides, Jacobs I, 63, 20
"""),
("""
Dead is she? No; rather let us call ourselves dead, who tire so soon in the service of the Master whom she has gone to serve forever.
""",
"""
W. S. Smart, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 184
"""),
("""
To our graves we walk, In the thick footprints of departed men.
""",
"""
Alexander Smith, Horton, line 570
"""),
("""
Tarry with me, O my Saviour!, Lay my head upon Thy breast,, Till the morning; then awake me —, Morning of eternal rest.
""",
"""
Caroline S. Smith, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 181
"""),
("""
Death is one of two things. Either it is annihilation, and the dead have no consciousness of anything; or, as we are told, it is really a change: a migration of the soul from this place to another.
""",
"""
Socrates, in Plato's Apology, 41
"""),
("""
Death is not the worst evil, but rather when we wish to die and cannot.
""",
"""
Sophocles Electra, 1007.
"""),
("""
Death! to the happy thou art terrible;, But how the wretched love to think of thee,, O thou true comforter! the friend of all, Who have no friend beside!
""",
"""
Robert Southey, Joan of Arc, Book I, line 318
"""),
("""
Death is the waiting-room where we robe ourselves for immortality.
""",
"""
Charles Spurgeon, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 180
"""),
("""
David: Lieutenant Saavik was right: you never have faced death --
""",
"""
Star Trek II: The Wrath of Khan, screenplay by Jack B. Sowards and Nicholas Meyer. Story by Harve Bennett, Jack B. Sowards, Nicholas Meyer and Samuel A. Peeples
"""),
("""
Death, for the author of the John Gospel as for the strict Moslem, is not the end of life, but a Something, a death-force, that contends with a life-force for the possession of man.
""",
"""
Oswald Spengler, The Decline of the West
"""),
("""
Death is an equall doome, To good and bad, the common In of rest.
""",
"""
Edmund Spenser, The Faerie Queene (1589-96), II. 59. Also III. 3. 30
"""),
("""
Death slue not him, but he made death his ladder to the skies.
""",
"""
Edmund Spenser, An Epitaph upon Philip Sydney, line 20
"""),
("""
A free man thinks of death least of all things; and his wisdom is a meditation not of death but of life.
""",
"""
Variant translation: A free man thinks of nothing less than of death; and his wisdom is a meditation not on death but on life.
"""),
("""
Life into death—life’s other shape; no rupture, only crossing.
""",
"""
Dejan Stojanovic in Circling, “Awakening of a Flower” (Sequence: “A Conversations with Atoms”)
"""),
("""
If birth is a manifestation of life, death is another.
""",
"""
Dejan Stojanovic in ”Death.”
"""),
("""
Life and death merge in greatness.
""",
"""
Dejan Stojanovic in The Shape, "Hush" (Sequence: "Big Chamber")
"""),
("""
They are both spectacular, life and death.
""",
"""
Dejan Stojanovic in The Shape, "Hush" (Sequence: "Big Chamber")
"""),
("""
The problem is, information degrades each time a human cell replenishes itself. Death is inherent in flesh people. It seems to be written into the basic program—a way, perhaps, of keeping the universe from filling up with old people.
""",
"""
Michael Swanwick, Ancient Engines, in Asimov's Science Fiction magazine (February 1999), reprinted in David G. Hartwell (ed.) Year's Best SF 5 (2000), page 428
"""),
("""
Death, if thou wilt, fain would I plead with thee:, Canst thou not spare, of all our hopes have built,, One shelter where our spirits fain would be, Death, if thou wilt?
""",
"""
Algernon Charles Swinburne, A Dialogue, Stanza 1
"""),
("""
For thee, O now a silent soul, my brother,, Take at my hands this garland and farewell., Thin is the leaf, and chill the wintry smell,, And chill the solemn earth, a fatal mother.
""",
"""
Algernon Charles Swinburne, Ave Atque Vale, Stanza 18
"""),
("""
And hands that wist not though they dug a grave,, Undid the hasps of gold, and drank, and gave,, And he drank after, a deep glad kingly draught:, And all their life changed in them, for they quaffed, Death; if it be death so to drink, and fare, As men who change and are what these twain were.
""",
"""
Algernon Charles Swinburne, Tristram of Lyonesse, The Sailing of the Swallow, line 789
"""),
("""
On the mountains of memory, by the world's wellsprings,, In all men's eyes,, Where the light of the life of him is on all past things,, Death only dies.
""",
"""
Algernon Charles Swinburne, "Super Flumina Babylonis", The Complete Works of Algernon C. Swinburne (1925), vol. 2, page 106
"""),
("""
An honorable death is better than a dishonorable life.
""",
"""
Tacitus, Agricola, XXXIII
"""),
("""
Trust not your own powers till the day of your death.
""",
"""
Talmud, Aboth, 2
"""),
("""
Death is not rare, alas! nor burials few,, And soon the grassy coverlet of God, Spreads equal green above their ashes pale.
""",
"""
Bayard Taylor, The Picture of St. John, Book III, Stanza 84
"""),
("""
He that would die well must always look for death, every day knocking at the gates of the grave; and then the gates of the grave shall never prevail upon him to do him mischief.
""",
"""
Jeremy Taylor, Holy Dying, Chapter II, Part I
"""),
("""
The great world's altar-stairs, That slope thro' darkness up to God.
""",
"""
Alfred Tennyson, In Memoriam A.H.H. (1849), Part LV
"""),
("""
Death has made, His darkness beautiful with thee.
""",
"""
Alfred Tennyson, In Memoriam A.H.H. (1849), LXXIV
"""),
("""
God's finger touched him, and he slept.
""",
"""
Alfred Tennyson, In Memoriam A.H.H. (1849), LXXXV
"""),
("""
But O! for the touch of a vanished hand,, And the sound of a voice that is still!
""",
"""
Alfred Tennyson, Break, Break, Break
"""),
("""
Sunset and evening star,, And one clear call for me!, And may there be no moaning of the bar, When I put out to sea.
""",
"""
Alfred Tennyson, Crossing the Bar
"""),
("""
Twilight and evening bell,, And after that the dark!, And may there be no sadness of farewell, When I embark.
""",
"""
Alfred Tennyson, Crossing the Bar
"""),
("""
For tho' from out our bourne of Time and Place, The flood may bear me far,, I hope to see my Pilot face to face, When I have crossed the bar.
""",
"""
Alfred Tennyson, Crossing the Bar
"""),
("""
The night comes on that knows not morn,, When I shall cease to be all alone,, To live forgotten, and love forlorn.
""",
"""
Alfred Tennyson, Mariana in the South. Last stanza
"""),
("""
Whatever crazy sorrow saith,, No life that breathes with human breath, Has ever truly longed for death.
""",
"""
Alfred Tennyson, The Two Voices (1832; 1842), Stanza 132
"""),
("""
Sleep sweetly, tender heart, in peace;, Sleep, holy spirit, blessed soul,, While the stars burn, the moons increase,, And the great ages onward roll., Sleep till the end, true soul and sweet., Nothing comes to thee new or strange., Sleep full of rest from head to feet;, Lie still, dry dust, secure of change.
""",
"""
Alfred Tennyson, "To J. S." [James Spedding], stanzas 18–19, The Poetic and Dramatic Works of Alfred Lord Tennyson (1899), page 78
"""),
("""
Dead men bite not.
""",
"""
Theodotus, when counselling the death of Pompey; see Plutarch, Life of Pompey
"""),
("""
And at departure he will say, "Mayest thou rest soundly and quietly, and may the light turf lie easy on thy bones."
""",
"""
Tibullus, Camina, II. 4. 49
"""),
("""
I hear a voice you cannot hear,, Which says, I must not stay;, I see a hand you cannot see,, Which beckons me away.
""",
"""
Thomas Tickell, Colin and Lucy
"""),
("""
These taught us how to live; and (oh, too high, The price for knowledge!) taught us how to die.
""",
"""
Thomas Tickell, On the Death of Mr. Addison, line 81
"""),
("""
Since every day a little of our life is taken from us—since we are dying every day—the final hour when we cease to exist does not of itself bring death; it merely completes the death process.
""",
"""
Paul Tillich, The Courage To Be (1952), page 14
"""),
("""
Those who are lighthearted remind me of death.
""",
"""
Leo Tolstoy, “Buddhist Wisdom,” A Calendar of Wisdom, P. Sekirin, trans. (1997), November 5
"""),
("""
Our soul’s perfection is our life’s purpose; any other purpose, keeping death in mind, has no substance.
""",
"""
Leo Tolstoy, A Calendar of Wisdom, P. Sekirin, trans. (1997), November 23
"""),
("""
I believe if I should die,, And you should kiss my eyelids where I lie, Cold, dead, and dumb to all the world contains,, The folded orbs would open at thy breath,, And from its exile in the Isles of Death, Life would come gladly back along my veins.
""",
"""
Mary Ashley Townsend, Love's Belief (Credo), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
So what if a kid dies? God will take care of him.
""",
"""
Tamerlan Tsarnaev, in April 2013, as quoted in "Boston Bombing Day 1: The Stunning Stop the Killers Made After the Attack" (18 April 2016), by Brian Ross, ABC News
"""),
("""
Death is so preoccupied with life, that is has no time for anything else.
""",
"""
Mikhail Turovsky (b. 1933), Russian-American artist and aphorist. Itch of Wisdom (Cicuta Press, 1986)
"""),
("""
And God said, "A way must be conceived to pursue the dead beyond the tomb."
""",
"""
Mark Twain, Letters From the Earth (1909)
"""),
("""
Only the feeble resign themselves to final death and substitute some other desire for the longing for personal immortality. In the strong the zeal for perpetuity overrides the doubt of realizing it, and their superabundance of life overflows upon the other side of death.
""",
"""
Miguel de Unamuno, Tragic Sense of Life (1913)
"""),
("""
No one says in the morning: A day is soon past, let us wait for the night. ... Yet what we cannot be certain of for an hour, we sometimes feel assured of for life, and say: “If death is the end of everything, why give ourselves so much trouble?"
""",
"""
Vauvenargues, Reflections and Maxims, E. Lee, trans. (1903), page 173
"""),
("""
The supreme day has come and the inevitable hour.
""",
"""
Virgil, Æneid (29-19 BC), II. 324. Same in Lucan, VII. 197
"""),
("""
I have lived, and I have run the course which fortune allotted me; and now my shade shall descend illustrious to the grave.
""",
"""
Virgil, Æneid (29-19 BC), IV. 653
"""),
("""
The wave from which there is no return [the river Styx].
""",
"""
Virgil, Æneid (29-19 BC), VI. 425
"""),
("""
Is it then so sad a thing to die?
""",
"""
Virgil, Æneid (29-19 BC), XII. 646
"""),
("""
It is today, my dear, that I take a perilous leapage
""",
"""
Last words of Voltaire, quoting the words of King Henry to Gabrielle d'Estrées, when about to enter the Catholic Church, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
It is vain for the coward to flee; death follows close behind; it is only by defying it that the brave escape.
""",
"""
Voltaire, Le Triumvirat, IV. 7
"""),
("""
Everything was beautiful and nothing hurt.
""",
"""
Kurt Vonnegut, Slaughterhouse-Five (1969)
"""),
("""
Every man dies. Not every man really lives.
""",
"""
Randall Wallace, in lines for William Wallace in Braveheart
"""),
("""
But God, who is able to prevail, wrestled with him, as the angel did with Jacob, and marked him; marked him for his own.
""",
"""
Izaak Walton, Life of Donne, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Softly his fainting head he lay, Upon his Maker's breast;, His Maker kissed his soul away,, And laid his flesh to rest.
""",
"""
Isaac Watts, "Death of Moses", in Lyrics, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Hark! from the tombs a doleful sound.
""",
"""
Isaac Watts, "Funeral Thought", as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The tall, the wise, the reverend head,, Must lie as low as ours.
""",
"""
Isaac Watts, Hymns and Spiritual Songs, Book II, Hymn 63
"""),
("""
One may live as a conqueror, a king, or a magistrate; but he must die as a man.
""",
"""
Daniel Webster, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 173
"""),
("""
I know death hath ten thousand several doors, For men to take their exits.
""",
"""
John Webster, Duchess of Malfi, Act IV, scene 2
"""),
("""
I saw him now going the way of all flesh.
""",
"""
John Webster, Westward Ho! 2. 2
"""),
("""
Like Moses to thyself convey,, And kiss my raptured soul away.
""",
"""
Charles Wesley, Collection Hymn, 229. Folio 221
"""),
("""
Joy, shipmate, joy, (Pleased to my soul at death I cry,), Our life is closed, our life begins,, The long, long anchorage we leave,, The ship is clear at last, she leaps!, Joy, shipmate, joy!
""",
"""
Walt Whitman, Joy, Shipmate, Joy, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
O, I see now that life cannot exhibit all to me, as day cannot,, I see that I am to wait for what will be exhibited by death.
""",
"""
Walt Whitman, Night on the Prairies, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Nothing can happen more beautiful than death.
""",
"""
Walt Whitman, Starting from Paumanok, No. 12, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Death is nature's way of saying, "Your table is ready."
""",
"""
Robin Williams, as quoted in The Fourth—And by Far the Most Recent—637 Best Things Anybody Ever Said (1990) edited by Robert Byrne, p, 518
"""),
("""
It is not the fear of death, That damps my brow;, It is not for another breath, I ask thee now;, I could die with a lip unstirred.
""",
"""
Nathaniel Parker Willis; paraphrase of André's letter to Washington, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
How beautiful it is for a man to die, Upon the walls of Zion! to be called, Like a watch-worn and weary sentinel,, To put his armour off, and rest in heaven!
""",
"""
Nathaniel Parker Willis, On the Death of a Missionary, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
For I know that Death is a guest divine,, Who shall drink my blood as I drink this wine;, And he cares for nothing! a king is he—, Come on, old fellow, and drink with me!, With you I will drink to the solemn past,, Though the cup that I drain should be my last.
""",
"""
William Winter, Orgia, The Song of a Ruined Man
"""),
("""
But he lay like a warrior taking his rest,, With his martial cloak around him.
""",
"""
Charles Wolfe, The Burial of Sir John Moore, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
If I had thought thou couldst have died, I might not weep for thee;, But I forgot, when by thy side,, That thou couldst mortal be;, It never through my mind had passed,, That time would ever be over, When I on thee should look my last,, And thou shouldst smile no more!
""",
"""
Charles Wolfe, Song, The Death of Mary, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
O, sir! the good die first,, And they whose hearts are dry as summer dust, Burn to the socket.
""",
"""
William Wordsworth, The Excursion (1814), Book I
"""),
("""
Death is the quiet haven of us all.
""",
"""
William Wordsworth, as reported in, Dictionary of Burning Words of Brilliant Writers (1895) edited by Josiah Hotchkiss Gilbert, page 178
"""),
("""
"But they are dead; those two are dead!, Their spirits are in Heaven!", 'Twas throwing words away; for still, The little Maid would have her will,, And said, "Nay, we are seven!"
""",
"""
William Wordsworth, We Are Seven, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
He first deceased; she for a little tried, To live without him, liked it not, and died.
""",
"""
Sir Henry Wotton, On the Death of Sir Albert Morton's Wife, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Insatiate archer! could not one suffice?, Thy shaft flew thrice; and thrice my peace was slain!
""",
"""
Edward Young, Night Thoughts (1742-1745), Night I, line 212
"""),
("""
Who can take, Death's portrait? The tyrant never sat.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night II, line 52
"""),
("""
The chamber where the good man meets his fate, Is privileged beyond the common walk, Of virtuous life, quite in the verge of heaven.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night II, line 633
"""),
("""
A death-bed's a detector of the heart.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night II, line 641
"""),
("""
Lovely in death the beauteous ruin lay;, And if in death still lovely, lovelier there;, Far lovelier! pity swells the tide of love.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night III, line 104
"""),
("""
Death is the crown of life;, Were death denyed, poor man would live in vain;, Were death denyed, to live would not be life;, Were death denyed, ev'n fools would wish to die.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night III, line 523
"""),
("""
The knell, the shroud, the mattock and the grave,, The deep, damp vault, the darkness, and the worm.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night IV, line 10
"""),
("""
And feels a thousand deaths, in fearing one.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night IV, line 17
"""),
("""
As soon as man, expert from time, has found, The key of life, it opes the gates of death.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night IV, line 122
"""),
("""
Early, bright, transient, chaste, as morning dew, She sparkled, was exhaled, and went to heaven.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night V, line 600
"""),
("""
Death loves a shining mark, a signal blow.
""",
"""
Edward Young, Night Thoughts (1742-1745), Night V, line 1,011
"""),
("""
Men drop so fast, ere life's mid stage we tread,, Few know so many friends alive, as dead.
""",
"""
Edward Young, Love of Fame, line 97
"""),
("""
Death is mighty, and is no one's friend.
""",
"""
Roger Zelazny, Lord of Light (1967)
"""),
("""
Oh, stanch thy bootlesse teares, thy weeping is in vain;, I am not lost, for we in heaven shall one day meet againe.
""",
"""
"The Bride's Buriall", in Raxburghe Ballads edited by Charles Hindley, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
The death of one man is a tragedy, the death of millions is a statistic.
""",
"""
This saying may originate with "Französischer Witz" (1925) by Kurt Tucholsky.
"""),
("""
Xerxes the great did die;, And so must you and I.
""",
"""
New England Primer (1814), as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
Be happy while y'er leevin,, For y'er a lang time deid.
""",
"""
Scotch Motto for a house, in Notes and Queries, (7 December 1901), page 469; expression used by Edgar Wilson Nye, as reported in Hoyt's New Cyclopedia Of Practical Quotations (1922)
"""),
("""
A samurai once asked Zen Master Hakuin where he would go after he died. Hakuin answered "How am I supposed to know?", "How do you know? You're a Zen master!" exclaimed the samurai., "Yes, but not a dead one", Hakuin answered
""",
"""
Zen mondō, in Straight to the Heart of Zen: Eleven Classic Koans and Their Inner Meanings (2001) by Philip Kapleau
"""),
("""
Hail Cæsar, we who are about to die salute you.
""",
"""
The salutation of the gladiators on entering the arena, quoted in Tiberius Claudius Drusus, XXI. 13, by Suetonius
""")
]

HELP_TEXT = "The Death Quotes skill does one thing well: gives you a quote " +\
            "about death. Try saying something like Alexa, open Death Quotes."

def handler(event, context):
    output_speech = random.choice(QUOTES)[0].strip()
    # Check if we got a help request
    if event['request']['type'] == 'IntentRequest':
        if event['request']['intent']['name'] == 'AMAZON.HelpIntent':
            output_speech = HELP_TEXT
    return {
        'version': '1.0',
        'response': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': output_speech
                }
            }
        }

